import {
  AutocompleteArrayInput,
  AutocompleteInput,
  BooleanField,
  BooleanInput,
  Create,
  Datagrid,
  EditButton,
  FormTab,
  List,
  NullableBooleanInput,
  NumberInput,
  ReferenceArrayInput,
  ReferenceInput,
  SelectInput,
  TabbedForm,
  TextField,
  useRecordContext,
} from 'react-admin'
import { Edit, TextInput } from 'react-admin'
import {
  useCreateWithBackendValidations,
  useUpdateWithBackendValidations,
} from '../backendValidationsHooks'
import { useFormContext } from 'react-hook-form'
import axios from 'axios'

const customerFilters = [
  <TextInput source='q' label='Nome' alwaysOn resettable autoComplete='off' />,
  <NullableBooleanInput source='active' alwaysOn label='Ativo?' />,
]

const AddressField = () => {
  const record = useRecordContext()

  if (!record) return null

  return (
    <>
      <span>
        {record.address.street}, {record.address.number}&nbsp;-&nbsp;
        {record.address.city.name}, {record.address.city.state.abbr}
      </span>
    </>
  )
}

export const CustomerList = () => (
  <List filters={customerFilters} sort={{ field: 'name', order: 'ASC' }}>
    <Datagrid expand={<AddressField />} rowClick='edit'>
      <TextField source='name' label='Nome' />
      <BooleanField source='active' label='Ativo?' />
      <EditButton />
    </Datagrid>
  </List>
)

const CustomerForm = props => {
  return (
    <TabbedForm onSubmit={props.onSubmit}>
      <FormTab label='Dados Básicos'>
        <TextInput source='name' label='Nome' />
        <BooleanInput source='active' label='Ativo?' />
        <SelectInput
          source='kind'
          label='Tipo'
          translateChoice
          choices={[
            { id: 'personal', name: 'Pessoa Física' },
            { id: 'business', name: 'Pessoa Jurídica' },
          ]}
        ></SelectInput>
      </FormTab>
      <FormTab label='Preferências'>
        <ReferenceArrayInput
          reference='product_categories'
          source='preferred_product_category_ids'
        >
          <AutocompleteArrayInput
            optionText='name'
            translateChoice
            fullWidth
            label='Categorias de produtos'
          />
        </ReferenceArrayInput>
      </FormTab>
      <FormTab label='Endereço'>
        <AddressFields />
      </FormTab>
    </TabbedForm>
  )
}

const AddressFields = props => {
  const { setValue } = useFormContext()

  const locateAddressByZipCode = event => {
    const zipCode = event.target.value.replace(/\D/g, '')
    const isValidZipCode = /^\d{8}$/.test(zipCode)

    if (isValidZipCode)
      axios
        .get(`https://viacep.com.br/ws/${zipCode}/json/`)
        .then(response => {
          setValue('address.street', response.data.logradouro)
          setValue('address.number', '')
          setValue(
            'address.complement',
            `(cidade: ${response.data.localidade}-${response.data.uf})`
          )
          setValue('address.community', response.data.bairro)
          setValue('address.city_id', '')
        })
        .catch(console.log)
  }

  return (
    <>
      <TextInput
        source='address.zip_code'
        label='CEP'
        onChange={locateAddressByZipCode}
      />
      <TextInput source='address.street' label='Rua' fullWidth />
      <NumberInput source='address.number' label='Número' />
      <TextInput source='address.complement' label='Complemento' />
      <TextInput source='address.community' label='Bairro' />
      <ReferenceInput
        source='address.city_id'
        reference='cities'
        sort={{ field: 'name', order: 'ASC' }}
      >
        <AutocompleteInput
          label='Cidade'
          optionText={city => `${city.name}, ${city.state.abbr}`}
          translateChoice
          fullWidth
        />
        {/* <SelectInput
          optionText={city => `${city.name}, ${city.state}`}
          translateChoice
          fullWidth
          label='Cidade'
        /> */}
      </ReferenceInput>
    </>
  )
}

export const CustomerEdit = () => {
  const save = useUpdateWithBackendValidations('customers', '/customers')

  return (
    <Edit>
      <CustomerForm onSubmit={save} />
    </Edit>
  )
}

export const CustomerCreate = () => {
  const save = useCreateWithBackendValidations('customers', '/customers')

  return (
    <Create>
      <CustomerForm onSubmit={save} />
    </Create>
  )
}

// window.useGetList = useGetList
