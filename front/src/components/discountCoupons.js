import {
  BooleanField,
  BooleanInput,
  Create,
  Datagrid,
  DateField,
  DateInput,
  List,
  NumberField,
  NumberInput,
  TextField,
} from 'react-admin'
import { Edit, SimpleForm, TextInput } from 'react-admin'
import {
  useCreateWithBackendValidations,
  useUpdateWithBackendValidations,
} from '../backendValidationsHooks'
import { IconButton } from '@mui/material'
import ArrowBackIcon from '@mui/icons-material/ArrowBack' // https://mui.com/material-ui/material-icons/
import { useNavigate } from 'react-router-dom'
import { useCallback } from 'react'

const RESOURCE_METADATA = {
  name: 'discount_coupons',
  indexPath: '/discount_coupons',
}

const discountCouponFilters = [
  <TextInput
    source='q'
    label='Código'
    alwaysOn
    resettable
    autoComplete='off'
  />,
  <NumberInput source='discount_gte' label='Desconto (Mín.)' alwaysOn />,
  <NumberInput source='discount_lte' label='Desconto (Máx.)' alwaysOn />,
  <DateInput
    source='expiration_date_gte'
    label='Data de Expiração (Mín.)'
    alwaysOn
  />,
  <DateInput
    source='expiration_date_lte'
    label='Data de Expiração (Máx.)'
    alwaysOn
  />,
]

export const DiscountCouponList = () => (
  <List
    sort={{ field: 'updated_at', order: 'DESC' }}
    filters={discountCouponFilters}
  >
    <Datagrid rowClick='edit'>
      <DateField source='updated_at' label='Última Atualização' showTime />
      <TextField source='code' label='Código' />
      <NumberField source='discount' label='Desconto (em %)' />
      <DateField source='expiration_date' label='Data de Expiração' />
      <BooleanField source='expired?' label='Expirado?' sortable={false} />
    </Datagrid>
  </List>
)

const DiscountCouponForm = props => {
  const navigate = useNavigate()

  const handleClose = useCallback(() => {
    navigate(RESOURCE_METADATA.indexPath)
  }, [navigate])

  return (
    <>
      <SimpleForm onSubmit={props.onSubmit}>
        <TextInput source='code' label='Código' disabled />
        <NumberInput source='discount' label='Desconto (em %)' />
        <DateInput source='expiration_date' label='Data de Expiração' />
        <BooleanInput source='expired?' label='Expirado?' disabled />

        <IconButton onClick={handleClose} size='small'>
          <ArrowBackIcon />
        </IconButton>
      </SimpleForm>
    </>
  )
}

export const DiscountCouponEdit = () => {
  const save = useUpdateWithBackendValidations(
    RESOURCE_METADATA.name,
    RESOURCE_METADATA.indexPath
  )

  return (
    <Edit>
      <DiscountCouponForm onSubmit={save} />
    </Edit>
  )
}

export const DiscountCouponCreate = () => {
  const save = useCreateWithBackendValidations(
    RESOURCE_METADATA.name,
    RESOURCE_METADATA.indexPath
  )

  return (
    <Create>
      <DiscountCouponForm onSubmit={save} />
    </Create>
  )
}
