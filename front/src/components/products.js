import {
  AutocompleteInput,
  Datagrid,
  EditButton,
  FormTab,
  List,
  NumberField,
  NumberInput,
  ReferenceInput,
  RichTextField,
  SelectInput,
  TabbedForm,
  TextField,
} from 'react-admin'
import { Edit, TextInput } from 'react-admin'
import { RichTextInput } from 'ra-input-rich-text'
import { useUpdateWithBackendValidations } from '../backendValidationsHooks'

const productFilters = [
  <TextInput source='q' label='Nome' alwaysOn resettable autoComplete='off' />,
  <ReferenceInput
    source='category_id'
    reference='product_categories'
    sort={{ field: 'name', order: 'ASC' }}
    // label='Category'
    alwaysOn
  >
    <AutocompleteInput label='Categoria' />
  </ReferenceInput>,
  <NumberInput source='price_gte' label='Preço (Mín.)' resettable />,
  <NumberInput source='price_lte' label='Preço (Máx.)' resettable />,
]

export const ProductList = () => (
  <List filters={productFilters} sort={{ field: 'name', order: 'ASC' }}>
    <Datagrid
      expand={<RichTextField source='description' stripTags={false} />}
      rowClick='edit'
    >
      <TextField source='name' label='Nome' />
      <NumberField
        source='price'
        label='Preço'
        options={{
          style: 'currency',
          currency: 'USD',
        }}
        sx={{ fontWeight: 'bold' }}
      />
      <TextField source='category.name' label='Categoria' />
      {/* <ReferenceField
        source='category_id'
        reference='product_categories'
        label='Category'
        sortable={false}
      /> */}
      <EditButton />
    </Datagrid>
  </List>
)

const ProductForm = props => (
  <TabbedForm onSubmit={props.onSubmit}>
    <FormTab label='Dados Básicos'>
      <TextInput source='name' fullWidth label='Nome' />
      <RichTextInput
        source='description'
        label='Descrição'
        fullWidth
        multiline
      />
    </FormTab>
    <FormTab label='Dados Adicionais'>
      <NumberInput source='price' label='Preço' />
      <ReferenceInput source='category_id' reference='product_categories'>
        {/* <AutocompleteInput translateChoice isRequired fullWidth /> */}
        <SelectInput
          optionText='name'
          translateChoice
          fullWidth
          label='Categoria'
        />
      </ReferenceInput>
    </FormTab>
  </TabbedForm>
)

export const ProductEdit = () => {
  const save = useUpdateWithBackendValidations('products', '/products')

  return (
    <Edit>
      <ProductForm onSubmit={save} />
    </Edit>
  )
}
