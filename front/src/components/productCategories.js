import { Create, Datagrid, List, TextField } from 'react-admin'
import { Edit, SimpleForm, TextInput } from 'react-admin'
import {
  useCreateWithBackendValidations,
  useUpdateWithBackendValidations,
} from '../backendValidationsHooks'

const productCategoryFilters = [
  <TextInput source='q' label='Nome' alwaysOn resettable autoComplete='off' />,
]

export const ProductCategoryList = () => (
  <List sort={{ field: 'name', order: 'ASC' }} filters={productCategoryFilters}>
    <Datagrid rowClick='edit'>
      <TextField source='name' label='Nome' />
    </Datagrid>
  </List>
)

const ProductCategoryForm = props => (
  <SimpleForm onSubmit={props.onSubmit}>
    <TextInput source='name' label='Nome' />
  </SimpleForm>
)

export const ProductCategoryEdit = () => {
  const save = useUpdateWithBackendValidations(
    'product_categories',
    '/product_categories'
  )

  return (
    <Edit>
      <ProductCategoryForm onSubmit={save} />
    </Edit>
  )
}

export const ProductCategoryCreate = () => {
  const save = useCreateWithBackendValidations(
    'product_categories',
    '/product_categories'
  )

  return (
    <Create>
      <ProductCategoryForm onSubmit={save} />
    </Create>
  )
}
