import {
  ArrayField,
  Datagrid,
  DateField,
  Labeled,
  List,
  NumberField,
  TextField,
} from 'react-admin'
import { DateInput, Edit, SimpleForm, TextInput } from 'react-admin'

export const OrderList = () => (
  <List>
    <Datagrid rowClick='edit'>
      <NumberField
        source='amount.raw'
        label='Total'
        sortable={false}
        options={{ style: 'currency', currency: 'USD' }}
      />
      <DateField source='created_at' label='Data' />
    </Datagrid>
  </List>
)

export const OrderEdit = () => (
  <Edit>
    <SimpleForm>
      <DateInput source='created_at' label='Data de Emissão' disabled />
      <Labeled label='Itens' fullWidth>
        <ArrayField source='items'>
          <Datagrid>
            <TextField source='product.name' label='Produto' />
            <NumberField
              source='price'
              label='Preço Unitário'
              options={{ style: 'currency', currency: 'USD' }}
            />
            <NumberField source='quantity' label='Quantidade' />
            <NumberField source='discount' label='Desconto' />
            <NumberField
              source='amount.raw'
              label='Sub-total'
              options={{ style: 'currency', currency: 'USD' }}
            />
          </Datagrid>
        </ArrayField>
      </Labeled>
      <TextInput source='amount.raw' label='Total da Nota' disabled />
    </SimpleForm>
  </Edit>
)
