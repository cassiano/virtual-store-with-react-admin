import {
  AutocompleteInput,
  Datagrid,
  List,
  ReferenceInput,
  SelectInput,
  TextField,
  TextInput,
} from 'react-admin'
import { Edit, SimpleForm } from 'react-admin'

const cityFilters = [
  <TextInput
    source='q'
    label='Cidade'
    alwaysOn
    resettable
    autoComplete='off'
  />,
  <ReferenceInput
    source='state_id'
    reference='states'
    sort={{ field: 'name', order: 'ASC' }}
    alwaysOn
  >
    <AutocompleteInput label='Estado' optionText='abbr' />
  </ReferenceInput>,
]

export const CityList = () => (
  <List sort={{ field: 'name', order: 'DESC' }} filters={cityFilters}>
    <Datagrid rowClick='edit'>
      <TextField source='name' label='Cidade' />
      <TextField source='state.abbr' label='Estado' />
    </Datagrid>
  </List>
)

export const CityEdit = () => (
  <Edit>
    <SimpleForm>
      <TextInput source='name' />
      <ReferenceInput source='state_id' reference='states'>
        <SelectInput
          optionText='abbr'
          translateChoice
          // fullWidth
          label='Estado'
        />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
)
