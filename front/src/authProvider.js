import axios from 'axios'
import { apiUrl } from './constants'

axios.defaults.baseURL = apiUrl

const authProvider = {
  // send username and password to the auth server and get back credentials
  login: params =>
    axios
      .post('/authentication/login', params)
      .then(response => {
        localStorage.setItem('auth', JSON.stringify(response.data))
      })
      .catch(err => {
        if (err.response.statusText === 'Forbidden') {
          throw new Error('Dados de login inválidos')
        } else {
          throw new Error('Erro de rede')
        }
      }),

  // when the dataProvider returns an error, check if this is an authentication error
  checkError: error => {
    const status = error.status

    if (status === 401 || status === 403) {
      localStorage.removeItem('auth')

      return Promise.reject()
    }

    // other error code (404, 500, etc): no need to log out
    return Promise.resolve()
  },

  // when the user navigates, make sure that their credentials are still valid
  checkAuth: () =>
    localStorage.getItem('auth') ? Promise.resolve() : Promise.reject(),

  // remove local credentials and notify the auth server that the user logged out
  logout: () => {
    localStorage.removeItem('auth')

    return Promise.resolve()
  },

  // get the user's profile
  getIdentity: () => {
    try {
      const { name: fullName, avatar } = JSON.parse(
        localStorage.getItem('auth')
      )

      return Promise.resolve({ fullName, avatar })
    } catch (error) {
      return Promise.reject(error)
    }
  },

  // get the user permissions (optional)
  getPermissions: () => Promise.resolve(),
}

export default authProvider
