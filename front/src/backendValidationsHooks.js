import { useCreate, useRedirect, useUpdate } from 'react-admin'
import { useParams } from 'react-router-dom'

export const useUpdateWithBackendValidations = (resource, redirectToPath) => {
  const [update] = useUpdate()
  const { id } = useParams()

  return useSaveFnGenerator(update, resource, { id }, redirectToPath)
}

export const useCreateWithBackendValidations = (resource, redirectToPath) => {
  const [create] = useCreate()

  return useSaveFnGenerator(create, resource, {}, redirectToPath)
}

const useSaveFnGenerator = (
  createOrUpdateFn,
  resource,
  payload,
  redirectToPath
) => {
  const redirectTo = useRedirect()

  const save = data =>
    createOrUpdateFn(resource, { ...payload, data }, { returnPromise: true })
      .then(() => redirectTo(redirectToPath))
      .catch(error => {
        if (error.status === 422) {
          return error.body
        } else {
          console.log(`Unexpected error: ${error.status} - ${error.message}`)
        }
      })

  return save
}
