import * as React from 'react'
import {
  Admin,
  defaultTheme,
  EditGuesser,
  fetchUtils,
  Layout,
  ListGuesser,
  Resource,
} from 'react-admin'
import jsonServerProvider from 'ra-data-json-server'
import { ProductEdit, ProductList } from './components/products'
import {
  CustomerCreate,
  CustomerEdit,
  CustomerList,
} from './components/customers'
import { ToggleThemeButton, AppBar } from 'react-admin'
import { Typography } from '@mui/material'
import {
  ProductCategoryCreate,
  ProductCategoryEdit,
  ProductCategoryList,
} from './components/productCategories'
import { darkTheme, lightTheme } from './themes'
import { ptBrI18nProvider } from './ptBrI18nProvider'
import { CityEdit, CityList } from './components/cities'
import authProvider from './authProvider'
import { OrderEdit, OrderList } from './components/orders'
import { apiUrl } from './constants'
import {
  DiscountCouponCreate,
  DiscountCouponEdit,
  DiscountCouponList,
} from './components/discountCoupons'

const httpClient = (url, options = {}) => {
  if (!options.headers)
    options.headers = new Headers({ Accept: 'application/json' })

  const { token } = JSON.parse(localStorage.getItem('auth'))

  options.headers.set('Authorization', `Bearer ${token}`)

  return fetchUtils.fetchJson(url, options)
}

const dataProvider = jsonServerProvider(apiUrl, httpClient)

const MyAppBar = props => (
  <AppBar>
    <Typography flex='1' variant='h6' id='react-admin-title'></Typography>
    <ToggleThemeButton lightTheme={lightTheme} darkTheme={darkTheme} />
  </AppBar>
)

const myLayout = props => <Layout {...props} appBar={MyAppBar} />

const App = () => (
  <Admin
    dataProvider={dataProvider}
    authProvider={authProvider}
    theme={defaultTheme}
    layout={myLayout}
    i18nProvider={ptBrI18nProvider}
    // requireAuth
  >
    <Resource
      name='products'
      list={ProductList}
      edit={ProductEdit}
      recordRepresentation='name'
      options={{ label: 'Produtos' }}
    />
    <Resource
      name='product_categories'
      list={ProductCategoryList}
      edit={ProductCategoryEdit}
      create={ProductCategoryCreate}
      recordRepresentation='name'
      options={{ label: 'Categorias de produtos' }}
    />
    <Resource
      name='customers'
      list={CustomerList}
      edit={CustomerEdit}
      create={CustomerCreate}
      recordRepresentation='name'
      options={{ label: 'Clientes' }}
    />
    <Resource
      name='cities'
      list={CityList}
      edit={CityEdit}
      options={{ label: 'Cidades' }}
    />
    <Resource
      name='orders'
      list={OrderList}
      edit={OrderEdit}
      options={{ label: 'Pedidos' }}
    />
    <Resource
      name='discount_coupons'
      list={DiscountCouponList}
      edit={DiscountCouponEdit}
      create={DiscountCouponCreate}
      options={{ label: 'Cupons de Descontos' }}
    />
  </Admin>
)

export default App
