# For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post 'authentication/login'

      resources :products, only: %i[index show update destroy]
      resources :product_categories, only: %i[index show update create destroy]
      resources :customers, only: %i[index show update create]
      resources :customer_addresses, only: %i[index show]
      resources :cities, only: %i[index show update]
      resources :states, only: %i[index]
      resources :orders, only: %i[index show]
      resources :discount_coupons, only: %i[index show update create destroy]
    end
  end
end
