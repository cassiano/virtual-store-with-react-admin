# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_09_29_035637) do
  create_table "cities", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "state_id", null: false
    t.index ["name"], name: "index_cities_on_name_and_state", unique: true
    t.index ["state_id"], name: "index_cities_on_state_id"
  end

  create_table "customer_addresses", force: :cascade do |t|
    t.string "zip_code", null: false
    t.string "street", null: false
    t.integer "number", null: false
    t.string "complement"
    t.string "community", null: false
    t.integer "customer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "city_id"
    t.index ["city_id"], name: "index_customer_addresses_on_city_id"
    t.index ["customer_id"], name: "index_customer_addresses_on_customer_id", unique: true
    t.index ["street", "number"], name: "idx_customer_addresses_on_street_number_city_customer_id", unique: true
  end

  create_table "customers", force: :cascade do |t|
    t.string "name", null: false
    t.string "kind", limit: 2, null: false
    t.boolean "active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", null: false
    t.string "password", null: false
    t.string "avatar", null: false
    t.string "uuid", null: false
    t.index ["email"], name: "index_customers_on_email", unique: true
    t.index ["name"], name: "index_customers_on_name", unique: true
    t.index ["uuid"], name: "index_customers_on_uuid", unique: true
  end

  create_table "customers_product_categories", id: false, force: :cascade do |t|
    t.integer "customer_id", null: false
    t.integer "category_id", null: false
    t.index ["category_id"], name: "index_customers_product_categories_on_category_id"
    t.index ["customer_id", "category_id"], name: "idx_cust_prod_categories_on_category_id_and_customer_id", unique: true
  end

  create_table "discount_coupons", force: :cascade do |t|
    t.string "code", null: false
    t.date "expiration_date", null: false
    t.float "discount", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_discount_coupons_on_code", unique: true
  end

  create_table "order_items", force: :cascade do |t|
    t.integer "order_id", null: false
    t.integer "product_id", null: false
    t.integer "quantity", null: false
    t.decimal "price", precision: 11, scale: 2, null: false
    t.float "discount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["product_id"], name: "index_order_items_on_product_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "customer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_orders_on_customer_id"
  end

  create_table "product_categories", force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_product_categories_on_name", unique: true
  end

  create_table "products", force: :cascade do |t|
    t.string "name", limit: 128, null: false
    t.text "description", null: false
    t.decimal "price", precision: 11, scale: 2, null: false
    t.integer "category_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_products_on_category_id"
    t.index ["name", "category_id"], name: "index_products_on_name_and_category_id", unique: true
  end

  create_table "states", force: :cascade do |t|
    t.string "abbr"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "cities", "states"
  add_foreign_key "customer_addresses", "cities"
  add_foreign_key "customer_addresses", "customers"
  add_foreign_key "customers_product_categories", "customers", on_delete: :cascade
  add_foreign_key "customers_product_categories", "product_categories", column: "category_id", on_delete: :cascade
  add_foreign_key "order_items", "orders"
  add_foreign_key "order_items", "products"
  add_foreign_key "orders", "customers"
  add_foreign_key "products", "product_categories", column: "category_id"
end
