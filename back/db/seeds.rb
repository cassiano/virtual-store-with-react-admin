# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# To know more about Faker: https://github.com/faker-ruby/faker

Faker::Config.locale = :en

puts "Generating categories:\n"

categories = 100.times.map do
  print '.'

  ProductCategory.create! name: Faker::Commerce.unique.department
end

puts "\n\nGenerating products:\n"

products = 500.times.map do
  print '.'

  Product.create!(
    name: Faker::Commerce.unique.product_name,
    description: Faker::Lorem.sentence,
    price: Faker::Number.between(from: 0.01, to: 1000.00),
    category: categories.sample
  )
end

puts "\n\nGenerating states:\n"

states = 20.times.map do
  print '.'

  State.create! abbr: Faker::Address.state_abbr
end

puts "\n\nGenerating customers:\n"

customers = 200.times.map do
  print '.'

  Customer.create!(
    uuid: Digest::UUID.uuid_v4,
    name: Faker::Name.unique.name,
    kind: Customer.kinds.keys.sample,
    email: Faker::Internet.unique.email,
    password: Faker::Internet.password,
    avatar: Faker::Avatar.image(size: "40x40"),
    address: CustomerAddress.new(
      zip_code: Faker::Address.zip_code,
      street: Faker::Address.street_name,
      number: Faker::Address.building_number,
      community: Faker::Address.community,
      city: City.find_or_create_by!(
        name: Faker::Address.city,
        state: states.sample
      )
    ),
    preferred_product_categories: categories.sample(rand(20) + 1),
    active: [*([true] * 8), *([false] * 2)].sample    # [80% trues, 20% falses] random distribution.
  )
end

puts "\n\nGenerating orders:\n"

active_customers = Customer.active

orders = 250.times.map do
  print '.'

  active_customers.sample.orders << Order.new(
    items: (rand(10) + 1).times.map do    # Will range from 1 to 10
      OrderItem.new(
        product: products.sample,
        quantity: rand(50) + 1,           # Will range from 1 to 50
        discount: rand(1000) / 1000.0     # Will range from 0.000 to 0.999
      )
    end
  )
end

puts "\n\nDone!"
