class AddCredentialsToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :email, :string, null: false, index: { unique: true }
    add_column :customers, :password, :string, null: false
  end
end
