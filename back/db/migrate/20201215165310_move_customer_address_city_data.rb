class MoveCustomerAddressCityData < ActiveRecord::Migration[6.0]
  def change
    add_column :cities, :original_name, :string, limit: 96

    # Copy existing cities into new table. PS: temporarily keep the
    # city original name in order to facilitate a future join.
    execute <<~SQL
      INSERT INTO cities (
        original_name,
        name,
        state,
        created_at,
        updated_at
      )
      SELECT DISTINCT
        city,
		    TRIM(SUBSTR(city, 1, INSTR(city, ',') - 1)) AS city_name,
        TRIM(SUBSTR(city, INSTR(city, ',') + 1, LENGTH(city) - INSTR(city, ','))) AS state_name,
        datetime('now'),
        datetime('now')
      FROM
        customer_addresses
      ORDER BY
        state_name,
        city_name
    SQL

    add_column :customer_addresses, :city_id, :integer
    add_foreign_key :customer_addresses, :cities
    add_index :customer_addresses, :city_id

    # MySQL version:
    # execute <<~SQL
    #   UPDATE
    #     customer_addresses CA INNER JOIN cities C ON CA.city = C.original_name
    #   SET
    #     CA.city_id = C.id
    # SQL

    # SQLite3 version:
    execute <<~SQL
      UPDATE
        customer_addresses
      SET
        city_id = (
          SELECT
            id
          FROM
            cities
          WHERE
            original_name = customer_addresses.city
        )
    SQL

    remove_column :customer_addresses, :city
    remove_column :cities, :original_name   # Drop the temporary column.
  end
end
