class AddStateIdFkToCities < ActiveRecord::Migration[7.0]
  def change
    add_reference :cities, :state, foreign_key: true, null: false
  end
end
