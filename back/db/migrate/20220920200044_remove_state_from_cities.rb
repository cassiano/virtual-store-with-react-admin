class RemoveStateFromCities < ActiveRecord::Migration[7.0]
  def change
    remove_column :cities, :state
  end
end
