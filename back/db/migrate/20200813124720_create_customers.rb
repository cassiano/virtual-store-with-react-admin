class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :name, index: { unique: true }, null: false
      t.string :kind, limit: 2, null: false
      t.boolean :active, null: false, default: true

      t.timestamps
    end
  end
end
