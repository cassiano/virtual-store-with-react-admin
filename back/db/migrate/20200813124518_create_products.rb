class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :name, null: false, limit: 128
      t.text :description, null: false
      t.decimal :price, precision: 11, scale: 2, null: false
      t.references  :category,
                    foreign_key: { to_table: :product_categories },
                    index: true,
                    null: false

      t.timestamps
    end

    add_index :products, [:name, :category_id], unique: true
  end
end
