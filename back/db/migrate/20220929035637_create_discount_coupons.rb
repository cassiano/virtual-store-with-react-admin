# 1000.times { |i| DiscountCoupon.create expiration_date: Date.today + i, discount: rand(100) }

class CreateDiscountCoupons < ActiveRecord::Migration[7.0]
  def change
    create_table :discount_coupons do |t|
      t.string :code, null: false, index: { unique: true }
      t.date :expiration_date, null: false
      t.float :discount, null: false

      t.timestamps
    end
  end
end
