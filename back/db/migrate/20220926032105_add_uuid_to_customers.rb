class AddUuidToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :uuid, :string, null: false, index: { unique: true }
  end
end
