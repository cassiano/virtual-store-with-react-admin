class AddAvatarToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_column :customers, :avatar, :string, null: false
  end
end
