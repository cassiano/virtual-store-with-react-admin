class CreateCustomerAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :customer_addresses do |t|
      t.string :zip_code, null: false
      t.string :street, null: false
      t.integer :number, null: false
      t.string :complement
      t.string :community, null: false
      t.string :city, null: false
      t.references  :customer,
                    foreign_key: true,
                    index: { unique: true },    # Force 1x1 relationship!
                    null: false

      t.timestamps
    end

    add_index :customer_addresses,
              [:street, :number, :city],
              unique: true,
              name: :idx_customer_addresses_on_street_number_city_customer_id
  end
end
