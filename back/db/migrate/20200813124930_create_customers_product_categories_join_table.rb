class CreateCustomersProductCategoriesJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_table :customers_product_categories, id: false do |t|
      t.references  :customer,
                    foreign_key: { on_delete: :cascade },
                    index: false,
                    null: false

      t.references  :category,
                    foreign_key: {
                      to_table: :product_categories,
                      on_delete: :cascade
                      },
                    index: true,
                    null: false
    end

    add_index :customers_product_categories,
              [:customer_id, :category_id],
              unique: true,
              name: :idx_cust_prod_categories_on_category_id_and_customer_id
  end
end
