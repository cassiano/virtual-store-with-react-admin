class CreateOrderItems < ActiveRecord::Migration[6.0]
  def change
    create_table :order_items do |t|
      t.references :order, foreign_key: true, index: true, null: false
      t.references :product, foreign_key: true, index: true, null: false
      t.integer :quantity, null: false
      t.decimal :price, precision: 11, scale: 2, null: false
      t.float :discount, precision: 4, scale: 1

      t.timestamps
    end
  end
end
