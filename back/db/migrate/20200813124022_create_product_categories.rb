class CreateProductCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :product_categories do |t|
      t.string :name, index: { unique: true }, null: false, limit: 64

      t.timestamps
    end
  end
end
