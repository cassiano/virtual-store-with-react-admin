class Api::ApiController < ApplicationController
  include ActionController::HttpAuthentication::Token::ControllerMethods

  DEFAULT_PER_PAGE = 10
  MAX_PER_PAGE = 50
  AUTHENTICATION_METADATA = {
    model: Customer,
    columns: {
      username: :email,
      password: :password,
      identity: :uuid,
      name: :name,
      avatar: :avatar
    }
  }.freeze

  before_action :authenticate
  before_action :find_current, only: %i[show update destroy]

  protected

  def logged_user
    if logged_in?
      @logged_user ||= AUTHENTICATION_METADATA[:model].find_by!(
        AUTHENTICATION_METADATA[:columns][:identity] => @logged_in_identity
      )
    end
  end

  def logged_in?
    !!@logged_in_identity
  end

  def render_index(ar_relation, query_expression: nil, serializer_options: {})
    start, q, order, sort, limit = index_params
    ids, limit = extract_ids if query_string_contains_at_least_one_id

    ar_relation = apply_like_filter(ar_relation, q, query_expression.to_s) if q && query_expression
    ar_relation = ar_relation.where(id: ids) if ids

    ar_relation = ar_relation
      .offset(start)
      .limit(limit)
      .order(sort => order)

    add_total_count_header_to_response ar_relation

    render({ json: ar_relation }.merge(default_serializer_options.merge(serializer_options)))
  end

  def process_and_render_update(object, merged_params = {}, serializer_options: {})
    if object.update(update_params.merge(merged_params))
      render({ json: object }.merge(default_serializer_options.merge(serializer_options)))
    else
      render json: join_errors(object.errors), status: :unprocessable_entity
    end
  end

  def render_show(object, serializer_options: {})
    render({ json: object }.merge(default_serializer_options.merge(serializer_options)))
  end

  def process_and_render_create(ar_relation, merged_params = {}, serializer_options: {})
    object = ar_relation.new(create_params.merge(merged_params))

    if object.save
      render({ json: object, status: :created }.merge(default_serializer_options.merge(serializer_options)))
    else
      render json: join_errors(object.errors), status: :unprocessable_entity
    end
  end

  def process_and_render_destroy(object)
    object.destroy

    render json: object
  end

  def find_current
    raise NotImplementedError
  end

  def update_params
    raise NotImplementedError
  end

  def create_params
    raise NotImplementedError
  end

  def has_param?(name)
    params.has_key? name
  end

  # https://api.rubyonrails.org/classes/ActiveRecord/NestedAttributes/ClassMethods.html
  def replace_nested_associations_keys(parameters, nested_associations)
    cloned_parameters = parameters.dup

    replace_key = -> (key) do
      new_key_name = "#{key}_attributes".to_sym
      cloned_parameters[new_key_name] = cloned_parameters.delete(key)

      new_key_name
    end

    nested_associations.each do |association|
      if association.is_a?(Symbol)
        replace_key[association]
      elsif association.is_a?(Hash)
        association.each do |(key, value)|
          new_key_name = replace_key[key]

          cloned_parameters[new_key_name] = replace_nested_associations_keys(
            cloned_parameters[new_key_name],
            [value]
          )
        end
      else
        raise "Invalid association (#{association})"
      end
    end

    cloned_parameters
  end

  def replace_nested_associations_sort_keys(model, association_name)
    return unless params.has_key?(:_sort)

    table_name = model.reflect_on_association(association_name).table_name

    params[:_sort].sub! "#{association_name}.", "#{table_name}."
  end

  private

  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    begin
      authenticate_with_http_token do |token, _|
        payload = HashWithIndifferentAccess.new(
          JWT.decode(
            token,
            jwt_credentials[:hmac_secret],
            true,
            { algorithm: 'HS256' }
          )[0]
        )

        @logged_in_identity = (data = payload[:data]) && data[:identity]
      end
    rescue JWT::ExpiredSignature
    end
  end

  def render_unauthorized(realm = 'Application')
    self.headers['WWW-Authenticate'] = %(Token realm="#{realm}")

    render json: 'Bad credentials', status: :unauthorized
  end

  def index_params
    start = params[:_start].present? ? params[:_start].to_i : nil
    _end = params[:_end].present? ? params[:_end].to_i : nil
    q = params[:q]
    order = params[:_order].present? ? params[:_order] : :asc
    sort = params[:_sort].present? ? params[:_sort] : :id
    limit = [_end ? _end - start : DEFAULT_PER_PAGE, MAX_PER_PAGE].min

    [start, q, order, sort, limit]
  end

  def add_total_count_header_to_response(ar_relation)
    response.set_header('Access-Control-Expose-Headers', 'X-Total-Count')
    response.set_header('X-Total-Count', ar_relation.except(:offset, :limit, :order).count)
  end

  def query_string_contains_at_least_one_id
    request.query_string =~ /\bid=/
  end

  def extract_ids
    ids = request.query_string.split('&')
      .filter { |key_value_pair| key_value_pair =~ /\bid=/ }
      .map { |key_value_pair| key_value_pair[/id=(\d+)/, 1].to_i }

    [ids, MAX_PER_PAGE]
  end

  def apply_like_filter(ar_relation, q, query_expression)
    ar_relation.where "#{query_expression} LIKE ?", "%#{q}%"
  end

  def join_errors(errors)
    errors.messages.reduce({}) do |acc, (name, messages)|
      acc.merge name => messages.join(', ')
    end
  end

  def jwt_credentials
    @jwt_credentials ||= Rails.application.credentials.dig(:jwt)
  end

  def default_serializer_options
    { host_with_port: request.host_with_port }
  end
end