class Api::V1::ProductCategoriesController < Api::ApiController
  def index
    render_index ProductCategory, query_expression: :name
  end

  def show
    render_show @product_category
  end

  def update
    process_and_render_update @product_category
  end

  def create
    process_and_render_create ProductCategory
  end

  def destroy
    process_and_render_destroy @product_category
  end

  private

  def find_current
    @product_category = ProductCategory.find(params[:id])
  end

  def update_params
    params.permit :name
  end

  alias_method :create_params, :update_params
end
