class Api::V1::AuthenticationController < Api::ApiController
  skip_before_action :authenticate, only: :login

  def login
    object = AUTHENTICATION_METADATA[:model].find_by(
      AUTHENTICATION_METADATA[:columns][:username] => params[:username],
      AUTHENTICATION_METADATA[:columns][:password] => params[:password]
    )

    head :forbidden and return unless object

    expiration = (Time.current + jwt_credentials[:expiration] * 60).to_i if jwt_credentials[:expiration]

    token = JWT.encode(
      {
        data: { identity: object.send(AUTHENTICATION_METADATA[:columns][:identity]) },
        exp: expiration
      },
      jwt_credentials[:hmac_secret],
      'HS256'
    )

    render json: {
      token: token,
      name: (name_column = AUTHENTICATION_METADATA[:columns][:name]) && object.send(name_column),
      avatar: (avatar_column = AUTHENTICATION_METADATA[:columns][:avatar]) && object.send(avatar_column)
    }
  end
end
