class Api::V1::CustomersController < Api::ApiController
  def index
    ar_relation = scoped_customer
    ar_relation = ar_relation.includes({ address: { city: :state } }, :preferred_product_categories)
    ar_relation = ar_relation.where(active: params[:active]) if has_param?(:active)

    render_index(
      ar_relation,
      query_expression: :name,
      serializer_options: { include: { address: { city: :state } } }
    )
  end

  def show
    render_show @customer
  end

  def update
    process_and_render_update @customer
  end

  def create
    process_and_render_create Customer
  end

  private

  def scoped_customer
    Customer #.where uuid: @logged_in_identity
  end

  def find_current
    @customer = scoped_customer.find(params[:id])
  end

  def update_params
    replace_nested_associations_keys(
      params.permit(
        :name,
        :kind,
        :active,
        preferred_product_category_ids: [],
        address: [
          :id, :zip_code, :street, :number, :complement, :community, :city_id
        ]
      ),
      [:address]
    )
  end

  alias_method :create_params, :update_params
end
