class Api::V1::ProductsController < Api::ApiController
  def index
    ar_relation = Product
    ar_relation = ar_relation.includes(:category)
    ar_relation = ar_relation.where(category_id: params[:category_id]) if has_param?(:category_id)
    ar_relation = ar_relation.where('price >= ?', params[:price_gte]) if has_param?(:price_gte)
    ar_relation = ar_relation.where('price <= ?', params[:price_lte]) if has_param?(:price_lte)

    replace_nested_associations_sort_keys Product, :category

    render_index ar_relation, query_expression: :name
  end

  def show
    render_show @product
  end

  def update
    process_and_render_update @product
  end

  def destroy
    process_and_render_destroy @product
  end

  private

  def find_current
    @product = Product.find(params[:id])
  end

  def update_params
    params.permit :name, :description, :price, :category_id
  end
end
