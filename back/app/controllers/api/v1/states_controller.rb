class Api::V1::StatesController < Api::ApiController
  def index
    render_index State, query_expression: :abbr
  end
end
