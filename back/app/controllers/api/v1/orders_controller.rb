class Api::V1::OrdersController < Api::ApiController
  def index
    ar_relation = logged_user.orders.includes({ items: :product })

    render_index ar_relation, serializer_options: { include: [] }
  end

  def show
    render_show @order, serializer_options: { include: { items: :product } }
  end

  private

  def find_current
    @order = logged_user.orders.includes({ items: :product }).find(params[:id])
  end
end
