class Api::V1::CustomerAddressesController < Api::ApiController
  def index
    render_index CustomerAddress.includes(:city)
  end

  def show
    render_show @customer_address
  end

  private

  def find_current
    @customer_address = CustomerAddress.find(params[:id])
  end
end
