class Api::V1::DiscountCouponsController < Api::ApiController
  def index
    ar_relation = DiscountCoupon
    ar_relation = ar_relation.where('discount >= ?', params[:discount_gte]) if has_param?(:discount_gte)
    ar_relation = ar_relation.where('discount <= ?', params[:discount_lte]) if has_param?(:discount_lte)
    ar_relation = ar_relation.where('expiration_date >= ?', params[:expiration_date_gte]) if has_param?(:expiration_date_gte)
    ar_relation = ar_relation.where('expiration_date <= ?', params[:expiration_date_lte]) if has_param?(:expiration_date_lte)

    render_index ar_relation, query_expression: :code
  end

  def show
    render_show @discount_coupon
  end

  def update
    process_and_render_update @discount_coupon
  end

  def create
    process_and_render_create DiscountCoupon
  end

  def destroy
    process_and_render_destroy @discount_coupon
  end

  private

  def find_current
    @discount_coupon = DiscountCoupon.find(params[:id])
  end

  def update_params
    params.permit :discount, :expiration_date
  end

  alias_method :create_params, :update_params
end
