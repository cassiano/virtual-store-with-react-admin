class Api::V1::CitiesController < Api::ApiController
  def index
    ar_relation = City
    ar_relation = ar_relation.includes(:state)
    ar_relation = ar_relation.where(state_id: params[:state_id]) if has_param?(:state_id)

    replace_nested_associations_sort_keys City, :state

    render_index ar_relation, query_expression: 'cities.name'
  end

  def show
    render_show @city
  end

  def update
    process_and_render_update @city
  end

  private

  def find_current
    @city = City.find(params[:id])
  end

  def update_params
    params.permit :name, :state_id
  end
end
