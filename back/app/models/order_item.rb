# == Schema Information
#
# Table name: order_items
#
#  id         :integer          not null, primary key
#  discount   :float
#  price      :decimal(11, 2)   not null
#  quantity   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  order_id   :integer          not null
#  product_id :integer          not null
#
# Indexes
#
#  index_order_items_on_order_id    (order_id)
#  index_order_items_on_product_id  (product_id)
#
# Foreign Keys
#
#  order_id    (order_id => orders.id)
#  product_id  (product_id => products.id)
#
class OrderItem < ApplicationRecord
  validates :quantity, :price, presence: true
  validates :discount,
            numericality: {
              greater_than_or_equal_to: 0.00,
              less_than_or_equal_to: 1.00
            }
  validate :order_has_valid_free_items_count

  belongs_to :order
  belongs_to :product

  delegate :customer, to: :order
  delegate :address, to: :order
  delegate :category, to: :product

  def amount
    quantity * price * (1 - (discount || 0.00)) if valid?
  end

  # Copy product price to item.
  def product=(new_product)
    super.tap do
      self.price = new_product&.price
    end
  end

  def to_s
    "Product: #{product}, quantity: #{quantity}, price paid: #{price.to_f}, discount: #{discount}, item amount: #{amount.to_f}"
  end

  def free?
    discount == 1.00
    # amount == 0.00
  end

  def sibling_items
    order.items.reject { |item| item == self }
  end

  private

  def order_has_valid_free_items_count
    errors.add :order, 'Order cannot contain more than 1 free item' unless order&.valid_free_items_count?
  end
end
