# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  description :text             not null
#  name        :string(128)      not null
#  price       :decimal(11, 2)   not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :integer          not null
#
# Indexes
#
#  index_products_on_category_id           (category_id)
#  index_products_on_name_and_category_id  (name,category_id) UNIQUE
#
# Foreign Keys
#
#  category_id  (category_id => product_categories.id)
#
class Product < ApplicationRecord
  validates :name, presence: true, uniqueness: { scope: :category }
  validates :price, presence: true
  validates :price, numericality: { greater_than: 0.00 }, allow_blank: true
  validates :description, presence: true
  validates :category_id, presence: true

  belongs_to :category, class_name: 'ProductCategory', required: false
  has_many :order_items
  has_many :orders, through: :order_items

  def to_s
    "Name: #{name}, category: #{category}, price: #{price.to_f}"
  end
end
