# == Schema Information
#
# Table name: discount_coupons
#
#  id              :integer          not null, primary key
#  code            :string           not null
#  discount        :float            not null
#  expiration_date :date             not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_discount_coupons_on_code  (code) UNIQUE
#
class DiscountCoupon < ApplicationRecord
  validates :code, presence: true, uniqueness: true
  validates :discount, presence: true, numericality: { greater_than: 0, less_than: 100 }
  validates :expiration_date, presence: true
  validate :expiration_date_cannot_be_in_the_past, on: :create

  before_validation :generate_code, on: :create

  def expired?
    return false unless expiration_date

    expiration_date < Date.today
  end

  private

  def expiration_date_cannot_be_in_the_past
    errors.add :expiration_date, "deve ser maior ou igual à data atual" if expired?
  end

  def generate_code
    self.code ||= 'xxxxxx-xxxxxx'.gsub('x') { rand(16).to_s(16).upcase }
  end
end
