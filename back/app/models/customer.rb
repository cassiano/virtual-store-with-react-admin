# == Schema Information
#
# Table name: customers
#
#  id         :integer          not null, primary key
#  active     :boolean          default(TRUE), not null
#  avatar     :string           not null
#  email      :string           not null
#  kind       :string(2)        not null
#  name       :string           not null
#  password   :string           not null
#  uuid       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_customers_on_email  (email) UNIQUE
#  index_customers_on_name   (name) UNIQUE
#  index_customers_on_uuid   (uuid) UNIQUE
#
class Customer < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :kind, presence: true

  has_one :address, class_name: 'CustomerAddress'
  has_and_belongs_to_many :preferred_product_categories,
                          class_name: 'ProductCategory',
                          association_foreign_key: :category_id
  has_many :orders

  enum kind: { personal: 'PF', business: 'PJ' }, _suffix: true

  delegate :street, :number, :city, to: :address

  accepts_nested_attributes_for :address

  scope :active, -> { where active: true }
  scope :inactive, -> { where active: false }

  def to_s
    "Name: #{name}, type: #{kind}"
  end

  def order_count
    orders.count
  end
end
