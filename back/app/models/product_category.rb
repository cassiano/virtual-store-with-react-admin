# == Schema Information
#
# Table name: product_categories
#
#  id         :integer          not null, primary key
#  name       :string(64)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_product_categories_on_name  (name) UNIQUE
#
class ProductCategory < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :products, foreign_key: :category_id
  has_and_belongs_to_many :customers, foreign_key: :category_id

  def to_s
    name
  end
end
