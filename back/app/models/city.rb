# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string(64)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  state_id   :integer          not null
#
# Indexes
#
#  index_cities_on_name_and_state  (name) UNIQUE
#  index_cities_on_state_id        (state_id)
#
# Foreign Keys
#
#  state_id  (state_id => states.id)
#
class City < ApplicationRecord
  validates :name, presence: true
  validates_uniqueness_of :name, scope: :state_id
  validates :state_id, presence: true

  has_many :customer_addresses
  belongs_to :state, required: false

  def full_name
    [name, state].join(', ')
  end

  def to_s
    full_name
  end

  # def xxx=(new_xxx)
  #   puts "xxx= called with parameter #{new_xxx}"
  # end

  # def yyy=(new_yyy)
  #   puts "yyy= called with parameter #{new_yyy}"
  # end
end
