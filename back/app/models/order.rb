# == Schema Information
#
# Table name: orders
#
#  id          :integer          not null, primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  customer_id :integer          not null
#
# Indexes
#
#  index_orders_on_customer_id  (customer_id)
#
# Foreign Keys
#
#  customer_id  (customer_id => customers.id)
#
class Order < ApplicationRecord
  belongs_to :customer
  has_many :items, class_name: 'OrderItem', autosave: true
  has_many :products, through: :items

  delegate :address, to: :customer

  validate :contains_at_least_one_item
  validate :references_an_active_customer, on: :create, if: :customer
  validate :does_not_contain_more_than_one_free_item
  validates_associated :items

  def amount
    # items.reduce(0.00) { |acc, item| acc + (item.amount || 0.00) }
    # items.reduce(0.00) { _1 + (_2.amount || 0.00) }
    # items.map(&:amount).compact.reduce(:+)
    # items.filter_map(&:amount).reduce(:+)
    # items.filter_map(&:amount).sum

    item_amounts = items.map(&:amount)

    item_amounts.sum if item_amounts.none?(&:nil?)
  end

  def to_s
    "Customer: #{customer}, items: [#{items.map(&:to_s).join('; ')}], order amount: #{amount.to_f}"
  end

  def valid_free_items_count?
    items.count(&:free?) < 2
    # items.count { |item| item.free? } < 2
  end

  private

  def references_an_active_customer
    errors.add :customer, "Cannot create orders for an inactive customer." unless customer.active?
  end

  def contains_at_least_one_item
    errors.add :items, "Order must have at least one item." if items.empty?
  end

  def does_not_contain_more_than_one_free_item
    errors.add :items, 'Cannot contain more than 1 free item' unless valid_free_items_count?
  end
end
