# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string(64)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  state_id   :integer          not null
#
# Indexes
#
#  index_cities_on_name_and_state  (name) UNIQUE
#  index_cities_on_state_id        (state_id)
#
# Foreign Keys
#
#  state_id  (state_id => states.id)
#
class CitySerializer < ActiveModel::Serializer
  attributes :id, :name, :state_id

  belongs_to :state
end
