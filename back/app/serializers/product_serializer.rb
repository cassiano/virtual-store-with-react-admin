# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  description :text             not null
#  name        :string(128)      not null
#  price       :decimal(11, 2)   not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :integer          not null
#
# Indexes
#
#  index_products_on_category_id           (category_id)
#  index_products_on_name_and_category_id  (name,category_id) UNIQUE
#
# Foreign Keys
#
#  category_id  (category_id => product_categories.id)
#
class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :price, :category_id

  belongs_to :category

  def price
    object.price.to_f
  end
end
