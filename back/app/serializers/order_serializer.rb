# == Schema Information
#
# Table name: orders
#
#  id          :integer          not null, primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  customer_id :integer          not null
#
# Indexes
#
#  index_orders_on_customer_id  (customer_id)
#
# Foreign Keys
#
#  customer_id  (customer_id => customers.id)
#
class OrderSerializer < ActiveModel::Serializer
  include ActionView::Helpers::NumberHelper
  include Rails.application.routes.url_helpers

  attributes :id, :amount, :created_at, :_links

  belongs_to :customer
  belongs_to :address
  has_many :items

  def _links
    {
      self: api_v1_order_url(object, host: instance_options[:host_with_port]),
      index: api_v1_orders_url(host: instance_options[:host_with_port])
    }
  end

  def amount
    {
      raw: object.amount.round(2),
      formatted: number_to_currency(object.amount, unit: 'R$ ')
    }
  end
end
