# == Schema Information
#
# Table name: product_categories
#
#  id         :integer          not null, primary key
#  name       :string(64)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_product_categories_on_name  (name) UNIQUE
#
class ProductCategorySerializer < ActiveModel::Serializer
  attributes :id, :name
end
