# == Schema Information
#
# Table name: discount_coupons
#
#  id              :integer          not null, primary key
#  code            :string           not null
#  discount        :float            not null
#  expiration_date :date             not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_discount_coupons_on_code  (code) UNIQUE
#
class DiscountCouponSerializer < ActiveModel::Serializer
  attributes :id, :created_at, :updated_at, :code, :discount, :expiration_date, :expired?
end
