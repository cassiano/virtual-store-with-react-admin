# == Schema Information
#
# Table name: order_items
#
#  id         :integer          not null, primary key
#  discount   :float
#  price      :decimal(11, 2)   not null
#  quantity   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  order_id   :integer          not null
#  product_id :integer          not null
#
# Indexes
#
#  index_order_items_on_order_id    (order_id)
#  index_order_items_on_product_id  (product_id)
#
# Foreign Keys
#
#  order_id    (order_id => orders.id)
#  product_id  (product_id => products.id)
#
class OrderItemSerializer < ActiveModel::Serializer
  include ActionView::Helpers::NumberHelper

  attributes :id, :price, :quantity, :discount, :amount

  belongs_to :product

  def amount
    {
      raw: object.amount.round(2),
      formatted: number_to_currency(object.amount, unit: 'R$ ')
    }
  end
end
