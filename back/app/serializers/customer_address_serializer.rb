# == Schema Information
#
# Table name: customer_addresses
#
#  id          :integer          not null, primary key
#  community   :string           not null
#  complement  :string
#  number      :integer          not null
#  street      :string           not null
#  zip_code    :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  city_id     :integer
#  customer_id :integer          not null
#
# Indexes
#
#  idx_customer_addresses_on_street_number_city_customer_id  (street,number) UNIQUE
#  index_customer_addresses_on_city_id                       (city_id)
#  index_customer_addresses_on_customer_id                   (customer_id) UNIQUE
#
# Foreign Keys
#
#  city_id      (city_id => cities.id)
#  customer_id  (customer_id => customers.id)
#
class CustomerAddressSerializer < ActiveModel::Serializer
  attributes :id, :zip_code, :street, :number, :complement, :community, :city_id

  belongs_to :city
end
