# == Schema Information
#
# Table name: states
#
#  id         :integer          not null, primary key
#  abbr       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class StateSerializer < ActiveModel::Serializer
  attributes :id, :abbr
end
