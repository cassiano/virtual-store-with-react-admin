# == Schema Information
#
# Table name: customers
#
#  id         :integer          not null, primary key
#  active     :boolean          default(TRUE), not null
#  avatar     :string           not null
#  email      :string           not null
#  kind       :string(2)        not null
#  name       :string           not null
#  password   :string           not null
#  uuid       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_customers_on_email  (email) UNIQUE
#  index_customers_on_name   (name) UNIQUE
#  index_customers_on_uuid   (uuid) UNIQUE
#
class CustomerSerializer < ActiveModel::Serializer
  attributes :id, :name, :kind, :active, :preferred_product_category_ids

  has_one :address
end
